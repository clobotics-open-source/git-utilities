from collections import defaultdict, deque
from datetime import datetime, timedelta
import functools
from operator import attrgetter, itemgetter
import re

import numpy as np
import requests
from tqdm import tqdm

from task import Task

GITLAB_API_URL = 'https://gitlab.com/api/v4'
# Regex matching constants
DATE = r'\d{4}-\d{2}-\d{2}'
DEPENDENCY = r'/depend .+\n?'
DIGIT = r'#\d+'
START = r'/start .+\n?'
URL = r'https?[:/.\w]+'

def getTasks(root, keyword, milestone_iid, headers):
    '''Parses a GitLab milestone to generate an ordered list of tasks
    for drawing a Gantt Chart.
    
    Arguments:
        project: Numeric id or URL encoded full path of GitLab project
        milestone_iid: GitLab project's milestone iid
        headers: GitLab API Token to be used as request header
    Returns:
        An ordered dictionary that maps issue IIDs to a list of 
        Task objects based on chronological ordering.
    '''
    tasks = _createTaskFromIssues(root, keyword, milestone_iid, headers)

    # Generate dependency paths for all tasks
    uid = '{}:{}'.format(root, 0)
    ordered_tasks = _generateCriticalPath(tasks, deque([(tasks[uid], [], 0)]))
    # for k, v in ordered_tasks.items():
    #     print(k,v)
    return ordered_tasks

def _createTaskFromIssues(root, keyword, milestone_id, headers):
    # Get milestone metadata
    r = requests.get(
        url = '{}/{}/{}/milestones?iids[]={}' \
            .format(GITLAB_API_URL, keyword, root.replace('/', '%2F'), milestone_id), 
        headers = headers
    )

    milestone_id = r.json()[0]['id']
    title = r.json()[0]['title']
    starting_date = r.json()[0]['start_date']
    
    # Create Tasks representing milestone start and end
    starting_task = Task(title + ' Start', 'Milestone', 0, 0, starting_date)
    ending_task = Task(title + ' Finish', 'Milestone', 0, 0, starting_date)
    ending_task.due_date = r.json()[0]['due_date']
    tasks = {}
    
    # Generate Tasks for all milestone issues
    issues = []
    page = 0
    while True:
        r = requests.get(
            url = '{}/{}/{}/milestones/{}/issues?per_page=100&page={}' \
                .format(GITLAB_API_URL, keyword, root.replace('/', '%2F'), milestone_id, page),
            headers = headers
        )
        for item in r.json():
            # print(item['title'])
            pass
        issues += r.json()
        print(len(r.json()))
        if len(r.json()) < 100: break
        page += 1

    create_bar = tqdm(issues)
    create_bar.set_description("Creating Tasks")
    for issue in create_bar:
        # Note : Gitlab implicitly assumes 8 hr workdays
        owner = issue['assignee']['name'] if issue['assignee'] else ''
        time = issue['time_stats']['time_estimate'] / (60 * 60 * 8)
        if issue['state'] == 'closed':
            spent = time
        else:
            spent = issue['time_stats']['total_time_spent'] / (60 * 60 * 8)
        prio = ''
        if 'high' in issue['labels']:
            prio = 'high'
        elif 'medium' in issue['labels']:
            prio = 'medium'
        elif 'low' in issue['labels']:
            prio = 'low'

        issue_task = Task(
            issue['title'],
            'Issue',
            time,
            spent, 
            starting_date,
            issue['web_url'],
            prio,
            owner 
        )
        tokens = issue['web_url'].split('/')
        issue_root = '/'.join(
            tokens[tokens.index('gitlab.com') + 1 : tokens.index('issues')]
        )
        uid = '{}:{}'.format(issue_root, issue['iid'])
        tasks[uid] = issue_task
        if issue['title'] == 'Investigate long delay recognition of China environment':
            print("hi")
    exit()
    # Parse Issue Body dependencies
    parse_bar = tqdm(list(enumerate(tasks.items())))
    parse_bar.set_description("Generating Dependencies")
    for index, pair in parse_bar:
        key, task = pair
        print(r.json()[index]['description'])
        print(index, pair)
        for dependency in re.findall(DEPENDENCY, r.json()[index]['description']):
            # Match local project dependencies
            for match in re.findall(DIGIT, dependency):
                uid = '{}:{}'.format(key.split(':')[0], match[1:])
                parent = tasks.get(uid, None)
                if parent:
                    task.parents.append(parent)
                    parent.children.append(task)
            # Match global group dependencies
            for match in re.findall(URL, dependency):
                tokens = match.split('/')
                match_root = '/'.join(
                    tokens[tokens.index('gitlab.com') + 1 : tokens.index('issues')]
                )
                uid = '{}:{}'.format(match_root, tokens[-1])
                parent = tasks.get(uid, None)
                if parent:
                    task.parents.append(parent)
                    parent.children.append(task)
        for dependency in re.findall(START, r.json()[index]['description']):
            for match in re.findall(DATE, dependency):
                task.float = np.busday_count(task.starting_date, match)
        if not task.parents:
            starting_task.children.append(task)
            task.parents.append(starting_task)

    # Create dependencies for ending task
    ending_task.parents = [task for task in tasks.values() 
        if not task.children and task != ending_task]
    for parent in ending_task.parents:
        parent.children.append(ending_task)

    tasks['{}:{}'.format(root, 0)] = starting_task
    tasks['{}:{}'.format(root, -1)] = ending_task
    return tasks

def _generateCriticalPath(tasks, queue):
    _parseTasks(queue)

    # Sort each task's path by maximum cost path
    # Critical path is finish task's max path.
    for task in tasks.values():
        task.path_costs.sort(key=itemgetter(1), reverse=True)
        ending_time = int(max(task.path_costs, key=itemgetter(1))[1])
        starting_time = int(ending_time - task.time)
        task.starting_date = _addDays(datetime.strptime(task.starting_date, '%Y-%m-%d'), starting_time)
        task.ending_date = _addDays(task.starting_date, task.time)
        task.spent_date = _addDays(task.starting_date, task.spent)
        print('{}:{}, {}, {}, {}\n{}, {}, {}\n'.format(task.title, task.time, starting_time, ending_time, task.spent, 
            task.starting_date, task.ending_date, task.spent_date))
    name_sort = sorted(tasks.items(), key=lambda pair: pair[1].owner)
    kv = sorted(name_sort, key=lambda pair: pair[1].time)
    kv2 = sorted(kv, key=lambda pair: pair[1].starting_date)
    return dict(sorted(kv2, key=lambda pair: pair[1].ending_date))    

def _parseTasks(queue):
    while len(queue) > 0:
        task, path, cost = queue.popleft()
        if not task.children:  
            # pass
            print(path, cost)
        cost += task.time
        cost = max(cost, task.float + task.time)
        task.path_costs.append((path, cost))
        queue.extend([(child, path + [task.title], cost) for child in task.children])

def _addDays(date, business_days):
    while business_days > 0:
        # Saturday = 5, Sunday = 6
        if date.weekday() < 5:
            business_days -= 1
        date += timedelta(days=1)
    return date
    