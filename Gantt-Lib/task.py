class Task:
    
    def __init__(self, title, level, time, spent, starting_time, link='', prio='', owner=''):
        '''A task object that stores metadata and dependencies.
        
        Arguments:
            title: Task's title
            level: Task's hierarchy level
            time: Expected task duration, in number of days
            spent: Number of days spent
            link: URL to the task
            prio: Priority of task
            owner: Owner of the task
            closed: Task is closed (complete)
        '''

        self.title = title
        self.level = level
        self.starting_date = starting_time
        self.ending_date = None
        self.time = time
        self.spent = spent
        self.parents = []
        self.children = []
        self.path_costs = []
        self.link = link
        self.prio = prio
        self.owner = owner
        self.float = 0
    
    def __str__(self):
        return '\n'.join([
            'Title : ' + self.title,
            'Owner : ' + self.owner,
            'Level : ' + self.level,
            'Start : {}, End : {}'.format(
                str(self.starting_date), 
                str(self.ending_date)
            ), 
            'Expected_Time : {}, Spent : {}'.format(self.time, self.spent),
            'Link : ' + self.link,
            'Parents : ' + str([parent.title for parent in self.parents]),
            'Children : ' + str([child.title for child in self.children]),
            'Paths : ' + str(self.path_costs),
            ''
        ])

    def percentDone(self):
        if self.spent == 0 and self.time == 0:
            return '-'
        else:
            if self.time == 0:
                print('Issue {} has no time estimate.'.format(self.link))
                return '{0:.0%}'.format(0.0)
            return '{0:.0%}'.format(self.spent / self.time)