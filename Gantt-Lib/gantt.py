"""
Gantt.py
Main module of project
"""

import json
import argparse
from collections import defaultdict, deque, OrderedDict
from datetime import timedelta, datetime

import requests
import numpy as np
import pandas as pd
from tqdm import tqdm

from task import Task
from gitLabTaskGen import getTasks

UPPER_LEFT = '\u250f'
LOWER_LEFT = '\u2517'
UPPER_RIGHT = '\u2513'
LOWER_RIGHT = '\u251b'
VERTICAL_BAR = '\u2503'
HORIZONTAL_BAR = '\u2501'
CROSS = '\u254b'
DIAMOND = '\u2666'
DOWN_ARROW = '\u2193'
TEE = '\u252b'
HORIZONTAL_TEE = '\u2533'

BLANK_SQUARE = '\u25fb'
BLANK_LPOINTER = '\u25c1'
BLANK_RPOINTER = '\u25b7'

BLACK_SQUARE = '\u25fc'
BLACK_LPOINTER = '\u25c0'
BLACK_RPOINTER = '\u25b6'

BLANK_BAR_WITH_ENDS = [BLANK_LPOINTER] + [BLANK_SQUARE] * 4 + [BLANK_RPOINTER]
BLANK_BAR = [BLANK_SQUARE] * 5
BLANK_BAR_WITH_LEFT = [BLANK_LPOINTER] + [BLANK_SQUARE] * 4
BLANK_BAR_WITH_RIGHT = [BLANK_SQUARE] * 4 + [BLANK_RPOINTER]

BLACK_BAR_WITH_ENDS = [BLACK_LPOINTER] + [BLACK_SQUARE] * 4 + [BLACK_RPOINTER]
BLACK_BAR = [BLACK_SQUARE] * 5
BLACK_BAR_WITH_LEFT = [BLACK_LPOINTER] + [BLACK_SQUARE] * 4
BLACK_BAR_WITH_RIGHT = [BLACK_SQUARE] * 4 + [BLACK_RPOINTER]

def setupParser():
    parser = argparse.ArgumentParser()
    ex = parser.add_mutually_exclusive_group()
    ex.add_argument('--project_root', nargs=1, help='ID or full path of project')
    ex.add_argument('--group_root', nargs=1, help='ID or full path of group')
    parser.add_argument('milestone_iid', nargs=1, help='IID of milestone of project root')
    parser.add_argument('--parseTasks', action='store_true', help='Flag to parse subtasks in issues')
    parser.add_argument('header_file', nargs='?', help='Location of private token, defaults to token.json',
                        type=argparse.FileType('r'), default='token.json')
    return parser.parse_args()

if __name__ == '__main__':
    args = setupParser()
    headers = json.loads(args.header_file.read())
    milestone_iid = args.milestone_iid[0]
    if args.project_root:
        root = args.project_root[0]
        keyword = 'projects'
    else:
        root = args.group_root[0]
        keyword = 'groups'
    tasks = getTasks(root, keyword, milestone_iid, headers)
    
    s_uid = '{}:{}'.format(root, 0)
    e_uid = '{}:{}'.format(root, -1)
    starting_date = tasks[s_uid].starting_date
    ending_date = tasks[e_uid].ending_date
    due_date = tasks[e_uid].due_date
    dates = pd.date_range(
        starting_date, 
        end=max(ending_date, datetime.strptime(due_date, '%Y-%m-%d'))
    )

    columns = pd.Index(['Hierarchy', 'Root', 'Issue #', 'Workload', 'Assignee', 'Priority', 'Progress']).append(dates)
    rows = [task.title + str(i) for task in tasks.values() for i in range(2)]
    print(rows)
    exit()
    df = pd.DataFrame(None, columns=columns, index=rows, dtype=object)
    print(df.loc["Investigate long delay recognition of China environment0", :], " Investigate long delay recognition of China environment0",  tasks[s_uid].starting_date, starting_date)

    df = df.where((pd.notnull(df)), None)
    print(df.at["Investigate long delay recognition of China environment0", starting_date], " Investigate long delay recognition of China environment0",  tasks[s_uid].starting_date, starting_date)

    df.loc[:, dates] = [[[] for _ in range(len(dates))] for _ in range(len(rows))]
    print(df.at["Investigate long delay recognition of China environment0", starting_date], " Investigate long delay recognition of China environment0",  tasks[s_uid].starting_date, starting_date)
    df.at[tasks[s_uid].title + '0', dates[0]].extend(
        [DIAMOND, HORIZONTAL_BAR, UPPER_RIGHT]
    )
    df.at[tasks[e_uid].title + '0', dates[-1]].extend(
        ['   ', '     ', LOWER_LEFT, HORIZONTAL_BAR, DIAMOND]
    )
    workloads = defaultdict(lambda : defaultdict(int))
    print(df.at["Investigate long delay recognition of China environment0", starting_date], " Investigate long delay recognition of China environment0",  tasks[s_uid].starting_date)
    # Calculate Workloads
    start_to_due = workloads['Start To Due']
    today = datetime.today().strftime('%Y-%m-%d')
    start_to_due['spent'] = np.busday_count(starting_date.strftime('%Y-%m-%d'), today)
    start_to_due['total'] = np.busday_count(starting_date.strftime('%Y-%m-%d'), due_date)
    start_to_end = workloads['Start To End']
    start_to_end['spent'] = np.busday_count(starting_date.strftime('%Y-%m-%d'), today)
    start_to_end['total'] = np.busday_count(starting_date.strftime('%Y-%m-%d'), ending_date.strftime('%Y-%m-%d'))

    print(df.at["Investigate long delay recognition of China environment0", starting_date], " Investigate long delay recognition of China environment0",  tasks[s_uid].starting_date)
    pbar = tqdm(tasks.values())
    pbar.set_description("Drawing Tasks")
    for task in pbar:
        if task.level == 'Issue':
            if task.owner:
                workloads[task.owner]['count'] += 1
                workloads[task.owner]['spent'] += task.spent
                workloads[task.owner]['total'] += task.time
            else:
                workloads['Unassigned']['count'] += 1
                workloads['Unassigned']['spent'] += task.spent
                workloads['Unassigned']['total'] += task.time

        title = task.title
        time = task.time
        spent = task.spent

        # Prepare date slices
        starting_date = task.starting_date
        ending_date = task.ending_date - timedelta(days=1)
        spent_date = max(starting_date,task.spent_date - timedelta(days=1))   
        starting_date_po = starting_date + timedelta(days=1)
        ending_date_mo = ending_date - timedelta(days=1)
        if time > 1:
            if spent > 0:
                df.at[title + '0', starting_date] = BLACK_BAR_WITH_LEFT
            else:
                df.at[title + '0', starting_date] = BLANK_BAR_WITH_LEFT
            # print(starting_date_po, spent_date)
            black_subsection = df.loc[title + '0', starting_date_po:spent_date] 
            black_subsection[:] = [BLACK_BAR for _ in range(black_subsection.size)]
            cutoff = max(task.spent_date, starting_date_po)
            blank_subsection = df.loc[title + '0', cutoff:ending_date_mo]
            blank_subsection[:] = [BLANK_BAR for _ in range(blank_subsection.size)]

            if spent == time:
                df.at[title + '0', ending_date] = BLACK_BAR_WITH_RIGHT
            else:
                df.at[title + '0', ending_date] = BLANK_BAR_WITH_RIGHT
        elif time == 1:
            if spent == 1:
                df.at[title + '0', starting_date] = BLACK_BAR_WITH_ENDS[:]
            else:
                df.at[title + '0', starting_date] = BLANK_BAR_WITH_ENDS[:]
        
        if time > 0 or 'Finish' in task.title:
            for parent in task.parents:
                # Corner Cell
                corner_list = df.at[rows[rows.index(parent.title + '0')], starting_date]
                if not corner_list:
                    corner_list.extend(["   ", HORIZONTAL_BAR, UPPER_RIGHT])
                elif corner_list[2] == VERTICAL_BAR:
                    corner_list[1] = HORIZONTAL_BAR
                    corner_list[2] = TEE

                # Horizontal Cells
                for col in pd.date_range(start=parent.ending_date, 
                    end=starting_date - timedelta(days=1)):
                    hor_chars = df.at[rows[rows.index(parent.title + '0')], col]
                    if not hor_chars:
                        hor_chars.extend([HORIZONTAL_BAR] * 4)
                    elif hor_chars[2] == UPPER_RIGHT:
                        hor_chars[2] = HORIZONTAL_TEE
                        hor_chars.append(HORIZONTAL_BAR)
                    elif hor_chars[2] == VERTICAL_BAR:
                        hor_chars[1] = HORIZONTAL_BAR
                        hor_chars[2] = CROSS
                        hor_chars.append(HORIZONTAL_BAR)
                    elif hor_chars[2] == TEE:
                        hor_chars[2] = CROSS
                        hor_chars.append(HORIZONTAL_BAR)
                    
                # Vertical Cells
                for row in rows[rows.index(parent.title + '1'):rows.index(title + '0') - 1]:
                    curr_chars = df.at[row, starting_date]
                    
                    if not curr_chars:
                        curr_chars.extend(['   ', '     ', VERTICAL_BAR])
                    elif curr_chars[2] == UPPER_RIGHT:
                        curr_chars[2] = TEE
                  
        # Arrow Cells        
        if not (starting_date == tasks[s_uid].starting_date and time == 0): 
            df.at[rows[rows.index(title + '0') - 1], starting_date].extend(['   ', '     ', DOWN_ARROW])
        
    # for key, val in workloads.items():
    #     print('{}:{}/{}, {}'.format(key, val['spent'], val['total'], val['count'])) 

    df = df.applymap(lambda chars: ''.join(chars) if type(chars) == type([]) else chars)
    new_cols = [date.date().strftime('%y-%m-%d') for date in dates]
    df = df.rename(columns=dict(zip(dates, new_cols), inplace=True))
    writer = pd.ExcelWriter('test.xlsx', engine='xlsxwriter')
    df.to_excel(writer, 'Gantt Chart', encoding='string')
    worksheet = writer.sheets['Gantt Chart']

    df2 = pd.DataFrame(None, index=workloads.keys(), 
        columns=['Number of Issues', 'Time Spent', 'Remaining Work', 'Total'])
    df2.loc[:, 'Number of Issues'] = [workload['count'] for workload in workloads.values()] 
    df2.loc[:, 'Time Spent'] = [workload['spent'] for workload in workloads.values()] 
    df2.loc[:, 'Remaining Work'] = [workload['total'] - workload['spent'] for workload in workloads.values()]
    df2.loc[:, 'Total'] = [workload['total'] for workload in workloads.values()] 
    df2.to_excel(writer, 'Individual Workloads', encoding='string')
    workloads_sheet = writer.sheets['Individual Workloads']
    
    weekend_format = writer.book.add_format({'bg_color': '#D9D9D9'})
    curr_day_format = writer.book.add_format({'bg_color': '#FFFFBE'})
    due_date_format = writer.book.add_format({'bg_color':'DA9694'})
    # Set Lengths
    for index, column in enumerate(df.columns):
        series = df[column]
        length = max(8, len(str(series.name)))
        worksheet.set_column(index + 1, index + 1, length) 
        # Highlight weekends
        if index > 6:
            date = datetime.strptime(column, '%y-%m-%d')
            if date.weekday() >= 5:
                worksheet.set_column(index + 1, index + 1, None, weekend_format)
    # Highlight current day, due date
    index = df.columns.get_loc(datetime.today().strftime('%y-%m-%d'))
    worksheet.set_column(index + 1, index + 1, None, curr_day_format)
    due_date = datetime.strptime(due_date, '%Y-%m-%d')
    due_date_index = df.columns.get_loc(due_date.strftime('%y-%m-%d'))
    worksheet.set_column(due_date_index + 1, due_date_index + 1, None, due_date_format)

    # Row Column
    length = max(20, len(max(rows, key=len)) // 3)
    worksheet.set_column(0, 0, length)
    # Merge previously duplicated cells
    merge_format = writer.book.add_format({
        'bold': 1,
        'border': 1,
        'align': 'center',
        'valign': 'vcenter',
        'text_wrap': True
    })
    title_format = writer.book.add_format({
        'bold': 1,
        'border': 1,
        'align': 'left',
        'valign': 'vcenter',
        'text_wrap': True
    })

    keys = list(tasks.keys())
    values = list(tasks.values())
    for row in range(1, len(rows), 2):
        index = row // 2
        # Hyperlinked Title
        worksheet.merge_range(row, 0, row + 1, 0, rows[row][0:-1] , title_format)
        if values[index].link:
            worksheet.write_url(
                row, 
                0, 
                url=values[index].link, 
                cell_format=title_format,
                string=rows[row][0:-1]
            )
        # Hierarchy
        worksheet.merge_range(row, 1, row + 1, 1, values[index].level , merge_format)
        # Root
        tokens = keys[index].split(':')
        leaf = tokens[0].split('/')[-1]
        worksheet.merge_range(row, 2, row + 1, 2, leaf, merge_format)
        # Issue #
        id = tokens[-1] if int(tokens[-1]) > 0 else '-'
        worksheet.merge_range(row, 3, row + 1, 3, tokens[-1], merge_format)
        # Workload    
        worksheet.merge_range(row, 4, row + 1, 4, values[index].time, merge_format)
        # Assignee
        worksheet.merge_range(row, 5, row + 1, 5, values[index].owner, merge_format)
        # Priority
        worksheet.merge_range(row, 6, row + 1, 6, values[index].prio, merge_format)
        # Progress
        worksheet.merge_range(row, 7, row + 1, 7, values[index].percentDone(), merge_format)
    # Freeze 1st row and first 4 cols
    worksheet.freeze_panes(1,4)
   
    # Set Lengths for Sheet 2
    for index, column in enumerate(df2.columns):
        series = df2[column]
        length = max(8, len(str(series.name)))
        workloads_sheet.set_column(index + 1, index + 1, length) 
    # Row Column
    workloads_sheet.set_column(0, 0, 20)
    writer.save()
    exit()
    
    