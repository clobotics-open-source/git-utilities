# ==============================================================================
# Copyright (c) 2018 by Clobotics Corporation. 
#
# This file is subject to the terms and conditions defined in 'LICENSE', which 
# is a part of this source code package.
# ==============================================================================

import argparse
import urllib
import json
import requests
from math import log
from gitlab_util import Utilities, http

headers = None
git_api_url = 'https://gitlab.com/api/v4'
byte_mapping = ['B', 'KB', 'MB', 'GB']

# Get file size of given project
# Call GET /projects/:id?statistics=True
# keyword  "projects" or "groups"
# id       url encoded full path or numeric id
# results  dictionary to store project sizes
def git_getProjectSizes(keyword, id, results):
    fullpath = "/".join(id.split("%2F"))
    default_url = '{}/{}/{}?statistics=true&license=true'.format(git_api_url, keyword, id)
    # print(default_url)
    r = http.get(url = default_url, headers = headers)
    # print(r.json())
    results[fullpath] = r.json()['statistics']['repository_size']

# Sets up an argument parser to read header information from file
def setupParser():
    parser = argparse.ArgumentParser()
    parser.add_argument('root', nargs=1, help='List all sizes of given group path and its subgroups/projects', metavar='root')
    parser.add_argument('header_file', nargs='?', help='Location of private token, defaults to token.json',
                        type=argparse.FileType('r'), default='token.json')
    return parser.parse_args()

if __name__ == '__main__':
    args = setupParser()
    headers = json.loads(args.header_file.read())
    utils = Utilities(headers)
    results = utils.parseTree(
        urllib.parse.quote_plus(args.root[0]), 
        git_getProjectSizes, 
        False, 
        {},
        p=True
    )

    # Sort results based on project repository size and convert sizes to B, KB, MB, etc.
    for project, size in sorted(results.items(), key=lambda item: (item[1],item[0])): 
        digits = int(log(size, 10)) + 1 if size else 1
        index = min(3, (digits - 1) // 3)
        print ("{} : {:03.2f} {}".format(project, size / float(1 << index * 10), byte_mapping[index]))


