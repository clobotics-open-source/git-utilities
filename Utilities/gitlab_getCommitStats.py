# ==============================================================================
# Copyright (c) 2021 by Clobotics Corporation.
#
# This file is subject to the terms and conditions defined in 'LICENSE', which
# is a part of this source code package.
# ==============================================================================

import argparse
import urllib
import json
from collections import defaultdict
from gitlab_util import Utilities, http
from xml.etree import ElementTree as ET
import datetime
import functools

headers = None
git_api_url = 'https://gitlab.com/api/v4'


def git_get_commit_stats(keyword, id, results, since, until):
    fullpath = "/".join(id.split("%2F"))
    default_url = f'{git_api_url}/{keyword}/{id}/repository/commits/?with_stats=true&per_page=100&since={since}&until={until}'
    commit_stats = []
    page =  1

    while True:
        try:
            r = http.get(url=default_url + '&page={}'.format(page), headers=headers)
        except:
            # We will implictly pass repos that can't be parsed.
            return results
        commit_stats += r.json()
        if len(r.json()) < 100: break
        page += 1

    for commit in commit_stats:
        if len(commit['parent_ids']) == 1:
            results[commit['author_name']][fullpath].append({
                "url": commit['web_url'],
                "stats": commit['stats'],
                "title": commit['title'],
                "authored_date": commit['authored_date']
            })
    return results

# Sets up an argument parser to read header information and root group to parse
def setupParser():
    parser = argparse.ArgumentParser()
    parser.add_argument('root', nargs=1, help='Generate all commit stats for group path and its subgroups/projects', metavar='root')
    parser.add_argument('header_file', nargs='?', help='Location of private token, defaults to token.json',
                        type=argparse.FileType('r'), default='token.json')
    parser.add_argument(
        '-s',
        '--since',
        type=int,
        help="Days ago, default 30",
        default=30
    )
    parser.add_argument(
        '-u',
        '--until',
        type=int,
        help="Days ago, default 0",
        default=0
    )
    return parser.parse_args()

if __name__ == '__main__':
    args = setupParser()
    headers = json.loads(args.header_file.read())
    utils = Utilities(headers)
    results = utils.parseTree(
        urllib.parse.quote_plus(args.root[0]),
        functools.partial(
            git_get_commit_stats,
            since=(datetime.datetime.now() - datetime.timedelta(days=args.since)).isoformat(),
            until=(datetime.datetime.now() - datetime.timedelta(days=args.until)).isoformat()
        ),
        False,
        defaultdict(lambda: defaultdict(list)),
        p=True
    )
    html = ET.Element('html')
    body = ET.Element('body')

    br = ET.Element('br')
    hr = ET.Element('hr')
    html.append(body)

    p = ET.Element('p')
    p.text = f"Commits from {args.since} days ago to {args.until} days ago since {datetime.datetime.now().strftime('%Y-%m-%d %H:%M:%S')}."
    body.extend([p, hr])

    for member in sorted(results, key=lambda s: s.casefold()):
        p = ET.Element('p')

        member_additions = 0
        member_deletions = 0
        member_total = 0
        per_project_blocks = []

        for project, stats in results[member].items():
            proj = ET.Element('p')
            proj_additions = 0
            proj_deletions = 0
            proj_total = 0

            for stat in stats:
                proj.append(br)
                entry = ET.Element('a', attrib = {'href': stat['url']})
                entry.text = f"{datetime.datetime.fromisoformat(stat['authored_date']).isoformat()} {stat['title']} {stat['stats']}"
                proj_additions += stat['stats'].get('additions', 0)
                proj_deletions += stat['stats'].get('deletions', 0)
                proj_total += stat['stats'].get('total', 0)
                proj.append(entry)

            per_project_blocks.extend([br, proj])
            proj.text = f"{project} - Additions {proj_additions}, Deletions {proj_deletions}, Total {proj_total}"
            member_additions += proj_additions
            member_deletions += proj_deletions
            member_total += proj_total

        p.text = f"{member} - Additions {member_additions}, Deletions {member_deletions}, Total {member_total}"
        per_project_blocks.append(br)
        p.extend(per_project_blocks)
        body.extend([p, hr])

    tree = ET.ElementTree(html)
    tree.getroot()
    tree.write(
        'commit_stats.html',
        encoding='unicode',
        method='html'
    )

