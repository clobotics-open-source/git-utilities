# ==============================================================================
# Copyright (c) 2018 by Clobotics Corporation. 
#
# This file is subject to the terms and conditions defined in 'LICENSE', which 
# is a part of this source code package.
# ==============================================================================

import argparse
import json
import requests
import urllib
from gitlab_util import http

headers = None
delete_success = 204
git_api_url = 'https://gitlab.com/api/v4'

# Remove members from given project/group, returns concatenated list of successfully removed names
# Call DELETE /groups/:id/members/:user_id, DELETE /projects/:id/members/:user_id
# keyword  "projects" or "groups"
# id       url encoded full path or numeric id
# member   numeric member id to delete from project/group
def git_removeMembers(keyword, id, member):
    default_url = '{}/{}/{}/members/{}'.format(git_api_url, keyword, id, member)
    r = http.delete(url=default_url, headers=headers)
    if r.status_code == delete_success:
        return True
    else:
        print(f"Insufficient privileges or already removed {member} from {id}")
    return False

# Sets up an argument parser to read header information and root group to parse
def setupParser():
    parser = argparse.ArgumentParser()
    parser.add_argument('member_file', nargs='?', help='Member list to find usernames from',
                        type=argparse.FileType('r'), default='members.txt')
    parser.add_argument('header_file', nargs='?', help='Location of private token, defaults to token.json',
                        type=argparse.FileType('r'), default='token.json')
    return parser.parse_args()

if __name__ == '__main__':
    args = setupParser()
    headers = json.loads(args.header_file.read())
    print(f"Using file {args.member_file.name}")
    lines =  list(args.member_file.readlines())
    candidates = []
    while len(candidates) == 0:
        s = input('--> Enter username to remove: ')

        for member_str in lines:
            if s in member_str:
                candidates.append(member_str)
        
        if len(candidates) > 0:
            for i, candidate in enumerate(candidates):
                print(f"{i + 1}: {candidate}")
        else:
            print("No candidates found, please try again")
    
    index = None
    while index is None or index >= len(candidates):
        index = int(input(f'--> Choose number from above options: ')) - 1
        if index >= len(candidates):
            print("Number out of range, please reselect")

    # Split getMembers output into individual components
    # Sample output: "callenc97 | Allen Chen | 3125115 | groups:['MyTest12345'] | projects:[]"
    split_data = candidates[index].split(' | ')
    gap = {split[0]: split[1].replace("\'", "\"") for split in [item.split(':') for item in split_data[3:]]}
    name = split_data[0]
    username = split_data[1]
    id = split_data[2]
    groups = json.loads(gap['groups'])
    projects = json.loads(gap['projects'])
    for group in groups:
        if git_removeMembers("groups", urllib.parse.quote_plus(group), id):
            print ("{} deleted from {} {}".format(name, "group", group))
    for project in projects:
        if git_removeMembers("projects", urllib.parse.quote_plus(project), id):
            print("{} deleted from {} {}".format(name, "project", project))
    print ("{} no longer present in any group/project".format(name))
