# ==============================================================================
# Copyright (c) 2018 by Clobotics Corporation. 
#
# This file is subject to the terms and conditions defined in 'LICENSE', which 
# is a part of this source code package.
# ==============================================================================

import argparse
import urllib
from xml.etree import ElementTree as ET
import json
import sys
import requests
from collections import defaultdict
from gitlab_util import Utilities, http

headers = None
git_api_url = 'https://gitlab.com/api/v4'

# Get members from given project/group, returns concatenated list of all its members
# Call GET /groups/:id/members, GET /projects/:id/members
# keyword  "projects" or "groups"
# id       url encoded full path or numeric id
# results  dictionary of dictionary of lists to store each member's projects/groups
#          (member -> project/group -> list of projects/groups) 
def git_getMembers(keyword, id, results):
    fullpath = "/".join(id.split("%2F"))
    default_url, members, page = '{}/{}/{}/members?per_page=100'.format(git_api_url, keyword, id), [], 1
    
    while True:
        r = http.get(url=default_url + '&page={}'.format(page), headers=headers)
        members += r.json()
        if len(r.json()) < 100: break
        page += 1

    for member in members:
        key = ' | '.join((member['name'], member['username'], str(member['id'])))
        results[key][keyword].append(fullpath)
    return results

# Sets up an argument parser to read header information and root group to parse
def setupParser():
    parser = argparse.ArgumentParser()
    parser.add_argument('root', nargs=1, help='List all members of given group path and its subgroups/projects', metavar='root')
    parser.add_argument('header_file', nargs='?', help='Location of private token, defaults to token.json',
                        type=argparse.FileType('r'), default='token.json')
    return parser.parse_args()

if __name__ == '__main__':
    args = setupParser()
    headers = json.loads(args.header_file.read())

    utils = Utilities(headers)
    results = utils.parseTree(
        urllib.parse.quote_plus(args.root[0]), 
        git_getMembers, 
        True, 
        defaultdict(lambda: defaultdict(list)), 
        p=True
    )

    html = ET.Element('html')
    body = ET.Element('body')
    html.append(body)
    
    for member in sorted(results, key=lambda s: s.casefold()):
        print("{} | groups:{} | projects:{}".format(
                member, 
                results[member]['groups'], 
                results[member]['projects']
            ),
            flush=True
        )
        url = "https://gitlab.com/dashboard/issues?assignee_username={}".format(member.split(' | ')[1])
        due_url = url + "&sort=due_date"
        update_url = url + "&sort=updated_desc&state=all"
        created_url = update_url.replace('assignee', 'author')
        activity_url = 'https://gitlab.com/users/{}/activity'.format(member.split(" | ")[1])
        mr_url = f'''https://gitlab.com/groups/{args.root[0]}/-/merge_requests?author_username={member.split(" | ")[1]}&scope=all&sort=updated_desc&state=all&utf8=%E2%9C%93'''
        p = ET.Element('p')
        p.text = member
      
        due = ET.Element('a', attrib = {'href': due_url})
        due.text = 'Due'

        last_updated = ET.Element('a', attrib = {'href': update_url})
        last_updated.text = 'Last Updated'

        br = ET.Element('br')

        activity = ET.Element('a', attrib = {'href': activity_url})
        activity.text = 'Activity'
        
        created_by = ET.Element('a', attrib = {'href': created_url})
        created_by.text = f"Created issues"
        mr = ET.Element('a', attrib = {'href' : mr_url})
        mr.text = 'Merge Requests'
        p.extend([br, due, br, last_updated, br, activity, br, created_by, br, mr, br])
        body.append(p)

    print(f"There are {len(results)} unique members.")
    ET.ElementTree(html).write(
        'members.html',
        encoding='unicode',
        method='html'
    )

