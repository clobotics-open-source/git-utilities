# ==============================================================================
# Copyright (c) 2018 by Clobotics Corporation. 
#
# This file is subject to the terms and conditions defined in 'LICENSE', which 
# is a part of this source code package.
# ==============================================================================

import json
import argparse
import urllib
import requests
from urllib3.util import Retry

retry_strategy = Retry(
    total=3,
    status_forcelist=[429, 500, 502, 503, 504],
    method_whitelist=["GET", "DELETE"],
    backoff_factor=0.5
)
adapter = requests.adapters.HTTPAdapter(max_retries=retry_strategy)
http = requests.Session()
http.mount("https://", adapter)
http.mount("http://", adapter)

# Utilities class for recursively parsing the gitlab directory tree.
class Utilities:
    # Initialize Utilities with header to provide in later requests
    # headers   Gitlab Access Token with format {'Private-Token': 'your-access-token'}
    def __init__(self, headers):
        self.headers = headers
        self.git_api_url = 'https://gitlab.com/api/v4'

    # Get subgroups from given group, returns concatenated list of all its subgroups
    # Call GET /groups/:id/subgroups
    # id        url encoded full path or numeric id
    def git_getSubgroups(self, id):
        default_url, results, page = '{}/groups/{}/subgroups?per_page=100'.format(self.git_api_url, id), [], 1
        while True:
            r = http.get(url=default_url + '&page={}'.format(page), headers=self.headers)
            results += r.json()
            if len(r.json()) < 100: break
            page += 1
        return results

    # Get projects from given group, returns concatenated list of all its projects
    # Call GET /groups/:id/projects
    # id        url encoded full path or numeric id
    def git_getProjects(self, id):
        default_url, results, page = '{}/groups/{}/projects?per_page=100'.format(self.git_api_url, id), [], 1
        while True:
            r = http.get(url=default_url + '&page={}'.format(page), headers=self.headers)
            results += r.json()
            if len(r.json()) < 100: break
            page += 1
        return results

    # Send paginated requests and concatenate resulting data as necessary
    # keyword   "projects" or "groups"
    # id        url encoded full path or numeric id
    # func      function to operate on leaf element of path, with args 
    #           (keyword, id, results)
    # results   data structure to store results of executing func
    # p		print flag
    def sendandConcatenateRequests(self, keyword, id, func, results, p=True):
        words = id.split("%2F")
        fullpath = "/".join(words)
        length = len(words)
        if p:
        	print("{}{}{}{} {}".format("\t" * (length - 1),
				 	"|" * (0 if length == 1 else 1), 
 				 	"_" * (0 if length == 1 else 1),
				 	" " + keyword[:-1], 
				 	fullpath), flush=True)
        func(keyword, id, results)   

    # Parses entire group directory, its subgroups, and its projects
    # id        url encoded full path or numeric id
    # func      function to operate on leaf element of path, with args 
    #           (keyword, id, results)
    # gflag     flag that enables func to be called on group elements as well
    # results   data structure to store results of executing func
    def parseTree(self, id, func, gflag, results, p=True):
        # Send request for group, if enabled
        if gflag:
            self.sendandConcatenateRequests("groups", id, func, results, p)
        # Parse subgroups recursively
        for subgroup in self.git_getSubgroups(id):
            self.parseTree(urllib.parse.quote_plus(subgroup['full_path']), func, gflag, results, p)
        # Parse non-archived projects iteratively
        for project in self.git_getProjects(id):
            if not project['archived']:
                self.sendandConcatenateRequests("projects", urllib.parse.quote_plus(project['path_with_namespace']), func, results,p)
        return results
