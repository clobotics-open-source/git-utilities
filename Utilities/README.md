**Token json file example:** 

{"Private-Token": "---------"}

Get access token from https://gitlab.com/profile/personal_access_tokens

Add to file Utilities/token.json.

**Show help:**

cd Utilities 
 
python3 gitlab_getMembers.py -h

**For listing users and their projects:**

python3 gitlab_getMembers.py Clobotics 

In addition, the script will produce 'members.html', a html page referring to each user and their activities.  

**Sample Listing:** 

python3 gitlab_getMembers.py Clobotics

... Directory parsing prints ...

test123 | Test123 | 123 | groups:[] | projects:['...', '...', '...']

There is 1 unique member.

**For removing numeric IDS:**

First, call the list option and tee the output (**I.e** python3 gitlab_getMembers.py | tee members.txt)

**I.e** Sample members.txt

test123 | Test123 | 123 | groups:[] | projects:['...', '...', '...']

**Sample Delete** 

python gitlab_removeMembers.py
--> Enter username to remove: test12  
1: test123 | Test123 | 123 | groups:[] | projects:['...', '...', '...']

--> Choose number from above options: 1
...  
Test123 no longer present in any group/project

