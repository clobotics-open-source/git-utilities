class Task:
    
    def __init__(self, title, project, workload, last_prog, current_prog, 
        start_date, due_date, is_new, url, closing_comment=''):
        '''A task object that stores metadata and dependencies.
        
        Arguments:
            title: Task's title
            level: Task's hierarchy level
            time: Expected task duration, in number of days
            spent: Number of days spent
            link: URL to the task
            prio: Priority of task
            owner: Owner of the task
            closed: Task is closed (complete)
        '''
        self.title = title
        self.project = project
        self.workload = str(workload)
        self.last_prog = last_prog
        self.current_prog = current_prog
        self.start_date = start_date
        self.due_date = due_date
        self.is_new = str(is_new)
        self.closing_comment = closing_comment
        self.url = url
    
    def __str__(self):
        return '\n'.join([

        ])
