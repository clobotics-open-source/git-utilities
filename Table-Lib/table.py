import argparse
from collections import defaultdict
from datetime import datetime, timedelta
import json
from math import ceil
import re

import requests
import pandas as pd
from tqdm import tqdm

from task import Task

ADD = re.compile('''added\s
    (?:(?P<months>\d+)mo\s?)? # Months
    (?:(?P<weeks>\d+)w\s?)? # Weeks
    (?:(?P<days>\d+)d\s?)? # Days
    (?:(?P<hours>\d+)h\s?)? # Hours
    (?:(?P<minutes>\d+)m\s?)? # Minutes
''', re.VERBOSE)
SUB = re.compile('''subtracted\s
    (?:(?P<months>\d+)mo\s?)? # Months
    (?:(?P<weeks>\d+)w\s?)? # Weeks
    (?:(?P<days>\d+)d\s?)? # Days
    (?:(?P<hours>\d+)h\s?)? # Hours
    (?:(?P<minutes>\d+)m\s?)? # Minutes
''', re.VERBOSE)
REM = re.compile('removed time spent')
START = r'/start .+\n?'
DATE = r'\d{4}-\d{2}-\d{2}'
GITLAB_API_URL = 'https://gitlab.com/api/v4'

def setupParser():
    parser = argparse.ArgumentParser()
    ex = parser.add_mutually_exclusive_group()
    ex.add_argument('--project_root', nargs=1, help='ID or full path of project')
    ex.add_argument('--group_root', nargs=1, help='ID or full path of group')
    parser.add_argument('milestone_iid', nargs=1, help='IID of milestone of project root')
    parser.add_argument('header_file', nargs='?', help='Location of private token, defaults to token.json',
                        type=argparse.FileType('r'), default='token.json')
    return parser.parse_args()

if __name__ == '__main__':
    args = setupParser()
    headers = json.loads(args.header_file.read())
    milestone = args.milestone_iid[0]
    if args.project_root:
        keyword = 'projects'
        root = args.project_root[0]
    else:
        keyword = 'groups'
        root = args.group_root[0]
    # root = 'Clobotics/Retail'
    r = requests.get(
        url = '{}/{}/{}/milestones?iids[]={}' \
            .format(GITLAB_API_URL, keyword, root.replace('/', '%2F'), milestone), 
        headers = headers
    ) 
    # print(r.json())
    milestone_id = r.json()[0]['id']
    
    issues = []
    page = 1
    while True:
        r2 = requests.get(
            url = '{}/{}/{}/milestones/{}/issues' \
                .format(GITLAB_API_URL, keyword, root.replace('/', '%2F'), milestone_id),
            headers = headers
        )
        # print(r2.json())
        issues += r2.json()
        if len(r2.json()) < 100: break
        page += 1
 
    tasks = defaultdict(list)
    stats = defaultdict(lambda: defaultdict(float))
    pbar = tqdm(issues)
    pbar.set_description("Filtering Issues")
    for issue in pbar:
        today = datetime.today()
        closed = issue['state'] == 'closed'
        has_estimate = issue['time_stats']['human_time_estimate'] == None

        if has_estimate:
            # Skip issues with no estimates
            continue

        stats[issue['assignee']['name']]['issues'] += 1
        if closed:
            stats[issue['assignee']['name']]['spent'] += \
                issue['time_stats']['time_estimate'] / (60 * 60 * 8)
        else:
            stats[issue['assignee']['name']]['spent'] += \
                issue['time_stats']['total_time_spent'] / (60 * 60 * 8)
        stats[issue['assignee']['name']]['total'] += \
            issue['time_stats']['time_estimate'] / (60 * 60 * 8)

        if closed: 
            closed_at = datetime.strptime(
                issue['closed_at'], 
                '%Y-%m-%dT%H:%M:%S.%fZ'
            )
            if today - closed_at > timedelta(days=7):
                # Filter issues closed last week
                continue
        
        comments = []
        page = 1
        while True:
            post_comments = requests.get(
                url = '{}/{}/{}/issues/{}/notes?per_page=100&page={}'.format(
                    GITLAB_API_URL, 'projects', issue['project_id'], issue['iid'], page
                ),
                headers = headers
            )
            comments += post_comments.json()
            if len(r.json()) < 100: break
            page += 1
        

        # Assignee
        assignee = issue['assignee']['name']
        # Title
        title = issue['title']
        # Project
        tokens = issue['web_url'].split('/')
        issue_root = '/'.join(
            tokens[tokens.index('gitlab.com') + 1 : tokens.index('issues')]
        )
        proj = '{}:{}'.format(issue_root, issue['iid'])
        # Workload
        workload = issue['time_stats']['time_estimate'] / (60 * 60 * 8)
        # URL
        url = issue['web_url']
        # Previous Progress
        lastweek = today - timedelta(days=7)
        spend_comments = [comment for comment in comments if
            ('time spent' in comment['body']) and
            (lastweek > datetime.strptime(comment['created_at'], '%Y-%m-%dT%H:%M:%S.%fZ'))
        ]
        total_spent = 0
        for spend_comment in spend_comments[::-1]:
            add = ADD.match(spend_comment['body'])
            if add:
                mo, w, d, h, m = add.group('months', 'weeks', 'days', 'hours', 'minutes')
                total_spent += 20 * float(mo or 0)
                total_spent += 5 * float(w or 0)
                total_spent += float(d or 0)
                total_spent += float(h or 0) / 8
                total_spent += float(m or 0) / 480
                continue

            sub = SUB.match(spend_comment['body'])
            if sub:
                mo, w, d, h, m = sub.group('months', 'weeks', 'days', 'hours', 'minutes')
                total_spent -= 20 * float(mo or 0)
                total_spent -= 5 * float(w or 0)
                total_spent -= float(d or 0)
                total_spent -= float(h or 0) / 8
                total_spent -= float(m or 0) / 480
                continue

            rem = REM.match(spend_comment['body'])
            if rem:
                total_spent = 0
        last_progress = '{0:.0%}'.format(total_spent / workload)

        # Current Progress
        if closed:
            spent = workload
        else:
            spent = issue['time_stats']['total_time_spent'] / (60 * 60 * 8)
        current_progress = '{0:.0%}'.format(spent / workload)

        # IsNew
        created_at = issue['created_at']
        created_date = datetime.strptime(created_at, '%Y-%m-%dT%H:%M:%S.%fZ')
        isNew = created_date > (today - timedelta(weeks=1))

        # Start Date
        starting_date = ''
        for dependency in re.findall(START, issue['description']):
            for date in re.findall(DATE, dependency):
                starting_date = date

        # DueDate
        if issue['due_date']:
            due_date = issue['due_date']
        else:
            due_date = ''

        # Closing Comment
        if closed:
            closing_comment = comments[0]['body']
            if closing_comment == 'closed':
                closing_comment = comments[1]['body']
        else:
            closing_comment = ''
        n_task = Task(title, proj, workload, last_progress, current_progress, starting_date, due_date, isNew, url, closing_comment)
        tasks[assignee].append(n_task)
    
    data = []
    for assignee, task_list in tasks.items():
        for task in task_list:
            task_dict = vars(task)
            task_dict['assignee'] = assignee
            data.append(task_dict)
    df = pd.DataFrame(data, columns=[
        'title', 'project', 'assignee', 'workload', 'last_prog', 'current_prog', 'is_new', 'start_date', 'due_date', 'closing_comment'
    ])
    writer = pd.ExcelWriter('test.xlsx', engine='xlsxwriter')
    df.to_excel(writer, 'Weekly Update')
    worksheet = writer.sheets['Weekly Update']

    cell_format = writer.book.add_format({'text_wrap': True, 'align': 'center',
        'valign': 'vcenter'})
    title_format = writer.book.add_format({
        'bold': 1,
        'border': 1,
        'align': 'left',
        'valign': 'vcenter',
        'text_wrap': True
    })

    # Set Row lengths
    for index, task_dict in enumerate(data):
        max_len = max(len(task_dict['title']), len(task_dict['closing_comment']))
        multiplier = ceil(max_len / 32)
        worksheet.set_row(index + 1, 15 * multiplier, cell_format)
        worksheet.write_url(
            row=index + 1, 
            col=1, 
            url=task_dict['url'], 
            cell_format=title_format,
            string=task_dict['title']
        )
    print(df)
    # Set Column lengths
    for index, column in enumerate(df.columns):
        series = df[column]
        max_len = max(len(column), len(max(df.loc[:, column], key=len)))
        length = min(32, max_len)
        worksheet.set_column(index + 1, index + 1, length) 

    worksheet.set_column(0, 0, None, cell_format)

    df2 = pd.DataFrame(None, index=stats.keys(), 
        columns=['Number of Issues', 'Time Spent', 'Remaining Work', 'Total'])
    df2.loc[:, 'Number of Issues'] = [workload['issues'] for workload in stats.values()] 
    df2.loc[:, 'Time Spent'] = [round(workload['spent'], 3) for workload in stats.values()] 
    df2.loc[:, 'Remaining Work'] = [round(workload['total'] - workload['spent'], 3)
        for workload in stats.values()]
    df2.loc[:, 'Total'] = [round(workload['total'], 3) for workload in stats.values()] 
    df2.to_excel(writer, 'Individual Workloads', encoding='string')
    workloads_sheet = writer.sheets['Individual Workloads']

    # Set Lengths for Sheet 2
    for index, column in enumerate(df2.columns):
        series = df2[column]
        length = max(8, len(str(series.name)))
        workloads_sheet.set_column(index + 1, index + 1, length) 

    workloads_sheet.set_column(0, 0, 20)

    writer.save()
    exit()
    

